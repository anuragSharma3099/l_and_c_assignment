from opencage_geocode import OpenCageGeoCodeApi
from exception import NoInternetConnectionException, GeoCodingApiException, InvalidAddressException
from utility import Utility
class GeoCode:
    def get_address_from_user(self):
        enter_address_string_literal = "Enter the address"
        return input(enter_address_string_literal + "\n")

    def get_latitude_and_longitude(self, response):
        try:
            result = response['results']
            latitude = result[0]['geometry']['lat']
            longitude = result[0]['geometry']['lng']
            return latitude, longitude
        except IndexError:
            invalid_address_string_literal = "Enter valid address"
            raise InvalidAddressException(invalid_address_string_literal)

    def display_latitude_and_longitude(self, response):
        latitide, longitude = self.get_latitude_and_longitude(response)
        latitude_string_literal = "Latitude is "
        longitude_string_literal = "Longitude is "
        print(latitude_string_literal, latitide)
        print(longitude_string_literal, longitude)

if __name__ == "__main__":
    geo_code_object = GeoCode()
    opencage_geocode_object = OpenCageGeoCodeApi()
    utility = Utility()
    while True:
        address = geo_code_object.get_address_from_user()
        try:
            api_response = opencage_geocode_object.get_response_from_api(address)
            utility.verify_status_code(api_response['status'])
            geo_code_object.display_latitude_and_longitude(api_response)
        except GeoCodingApiException as ex:
            print(ex)
        except NoInternetConnectionException as ex:
            print(ex)
        finally:
            print("----*----*----*----*----*----*----*----*----*----*")
