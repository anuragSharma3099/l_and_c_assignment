import unittest
import configparser
import requests
from python_wrap_cases import wrap_case

@wrap_case
class GeocodeApiTest(unittest.TestCase):

    def setUp(self):
        configuration = configparser.ConfigParser()
        configuration.read('configurationData.ini')
        key = configuration['OpenCageGeocodeApi']['key']
        self.url = configuration['OpenCageGeocodeApi']['url']
        self.params = {
            'key': key,
            'no_annotations': 1,
            'limit': 1
        }

    @wrap_case("india", 22.3511148, 78.6677428)
    def test_geocode_api_on_valid_address(self, address, mock_latitude, mock_longitude):
        self.params['q'] = address
        response = requests.get(self.url, params=self.params).json()
        result = response['results']
        latitude = result[0]['geometry']['lat']
        longitude = result[0]['geometry']['lng']
        self.assertEqual(latitude, mock_latitude, "Latitide value is wrong")
        self.assertEqual(longitude, mock_longitude, "Longitude value is wrong")

    @wrap_case(["1111111111111111111", "$%!@"], [])
    def test_geocode_api_for_invalid_address(self, addresses, mock_result):
        for address in addresses:
            self.params['q'] = address
            response = requests.get(self.url, params=self.params).json()
            result = response['results']
            self.assertEqual(result, mock_result, "address is valid")

    @wrap_case(["", "  ", "$"])
    def test_geocode_for_bad_request(self, addresses):
        for address in addresses:
            self.params['q'] = address
            response = requests.get(self.url, params=self.params).json()
            self.assertEqual(response['status']['code'], 400, "This request is not bad")

    @wrap_case(["iusgfueihghiu", "qeihfioeghoiu14"], "india")
    def test_geocode_for_unauthorized_access(self, invalid_keys, address):
        self.params['q'] = address
        for key in invalid_keys:
            self.params['key'] = key
            response = requests.get(self.url, params=self.params).json()
            self.assertEqual(response['status']['code'], 401, "This user is not a unauthorized user")

if __name__ == '__main__':
    unittest.main()
