class GeoCodingApiException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class NoInternetConnectionException(Exception):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class InvalidAddressException(GeoCodingApiException):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class BadRequestException(GeoCodingApiException):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class UnAuthorisedException(GeoCodingApiException):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class RateLimitExceededException(GeoCodingApiException):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)

class InternalServerException(GeoCodingApiException):
    def __init__(self, msg):
        self.msg = msg
        super().__init__(self.msg)
