from exception import BadRequestException, UnAuthorisedException, RateLimitExceededException, InternalServerException
class Utility:
    def verify_status_code(self, status):
        if status['code'] == 400:
            message = "Enter Valid Address"
            raise BadRequestException(message)
        elif status['code'] == 401:
            message = "You are not authorized. Please check the key value"
            raise UnAuthorisedException(message)
        elif status['code'] == 402:
            message = "Your limit if free usage is exhausted. Please do the payment before using it again"
            raise RateLimitExceededException(message)
        elif status['code'] == 500:
            message = "There is some internal issue in server"
            raise InternalServerException(message)
