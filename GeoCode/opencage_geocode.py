import configparser
import socket
import requests
from exception import NoInternetConnectionException
class OpenCageGeoCodeApi:
    def __init__(self):
        self.key, self.url = self.get_geocode_api_key_and_url()

    def get_geocode_api_key_and_url(self):
        configuration = configparser.ConfigParser()
        try:
            configuration.read('configurationData.ini')
            return configuration['OpenCageGeocodeApi']['key'], configuration['OpenCageGeocodeApi']['url']
        except KeyError as ex:
            error_string_literal = "Key Error due to"
            print(error_string_literal, ex)

    def get_response_from_api(self, address):
        params = {
            'key': self.key,
            'no_annotations': 1,
            'limit': 1,
            'q': address
        }
        try:
            response = requests.get(self.url, params=params).json()
            return response
        except socket.error:
            no_internet_message = "Please check your connection and try again"
            raise NoInternetConnectionException(no_internet_message)
