public class Customer { 
    private String firstName; 
    private String lastName; 
    private Wallet myWallet;
    public Customer(){
        this.myWallet = new Wallet();
    }
    public String getFirstName(){ 
        return this.firstName; 
    } 
    public String getLastName(){ 
        return this.lastName; 
    } 
    public void getPayment(float payment){
        String comeBackLaterMessage = "come back later and get your money";
        try{
            if(myWallet.getTotalMoney() >= payment){
                myWallet.subtractMoney(payment);
                System.out.println("Payment done successfully");
            }
            else{
            throw new InSufficientBalanceException(comeBackLaterMessage);
            }
        }
        catch(InSufficientBalanceException exception){
            System.out.println(comeBackLaterMessage);
        }
    } 
}