export class CardDetail {
  cardNumber: string;
  ownername: string;
  balance: number;
  status: string;
  pin: number;
}
