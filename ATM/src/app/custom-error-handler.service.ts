import { Injectable, Injector } from '@angular/core';
import { throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class CustomErrorHandlerService {
  private toastr?: ToastrService;
  constructor(private injector: Injector) { }

  handleError(error: any) {
    if (!this.toastr) {
      this.toastr = this.injector.get(ToastrService);
      }
    if (this.toastr) {
    if (error instanceof HttpErrorResponse) {
      if (error.error instanceof ErrorEvent) {
        console.log(error.error.message);
      } else {
        console.log('status: ' + error.status + '\nMessage: ' + error.message);
        if (error.status === 0) {
          this.toastr.error('Server Not Responding');
        } else {
        this.toastr.error(error.error);
        }
      }
    } else {
      this.toastr.error(error.message);
      console.log(error.message);
    }

    return throwError(error.message);
  }
}
}
