import { ApiService } from './Providers/api/api.service';
import { CardDetail } from './Models/CardDetails';
import { Component, OnInit } from '@angular/core';
import { CustomErrorHandlerService } from './custom-error-handler.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  message: string;
  isTransactionBegin: boolean;
  inputString: string;
  ATMBalance: number;
  isValidCard: boolean;
  isValidPIN: boolean;
  isValidAmount: boolean;
  amount: number;
  cardDetail: CardDetail ;
  countOfInvalidPIN: number;
  inputValue: string;
  constructor(private apiService: ApiService, private http: HttpClient) {

  }

  ngOnInit() {
    this.setMessage('Welcome to the XYZ Bank ATM');
    this.isTransactionBegin = false;
    this.ATMBalance = 30000;
    this.isValidCard = false;
    this.isValidPIN = false;
    this.isValidAmount = false;
    this.amount = 0;
    this.countOfInvalidPIN = 0;
    this.inputString = '';
    this.inputValue = '';
  }

  beginTransaction() {
    this.isTransactionBegin = true;
    this.setMessage('Enter your card number');
  }

  input(keyValue: string) {
    this.inputString += keyValue;
    this.inputValue += '*';
  }

  terminateTransaction() {
    this.setMessage('Welcome to the XYZ Bank ATM');
    this.isTransactionBegin = false;
    this.isValidCard = false;
    this.isValidPIN = false;
    this.countOfInvalidPIN = 0;
    this.amount = 0;
    this.isValidAmount = false;
    this.resetInput();
  }

  validateCard() {
    if (this.inputString.length !== 16) {
      this.inValidCard();
    }
    this.apiService.getCardDetails(this.inputString).subscribe(
      (data) => {
        this.cardDetail = data;
        this.setMessage('Enter the amount');
        this.resetInput();
        this.isValidCard = true;
      },
      (error) => {
        setTimeout(() => {this.terminateTransaction(); }, 3000);
      }
    );
  }

  validatePIN() {
    if (this.cardDetail.pin === Number(this.inputString)) {
      this.isValidPIN = true;
    }
    const body = {
      ATMBalance : this.ATMBalance,
      cashWithDrawal : this.amount,
      PINStatus : this.isValidPIN
    };
    this.apiService.updateCarddetails(this.cardDetail, body).subscribe(
    (data) => {
      this.ATMBalance -= this.amount;
      this.setMessage('Transaction Completed');
      this.resetInput();
      setTimeout(() => {this.terminateTransaction(); }, 3000);
    },
    (error) => {
      setTimeout(() => {this.terminateTransaction(); }, 3000);
    }
    );
  }

  validateAmount() {
    this.amount = Number(this.inputString);
    const body = {
      ATMBalance : -1,
      cashWithDrawal : this.amount,
      PINStatus : this.isValidPIN
    };
    this.apiService.updateCarddetails(this.cardDetail, body).subscribe(
    (data) => {
      this.isValidAmount = true;
      this.resetInput();
      this.message = 'Enter PIN';
    },
    (error) => {
      setTimeout(() => {this.terminateTransaction(); }, 3000);
    }
    );
  }

  operations() {
    if (!this.isValidCard) {
      this.validateCard();
    } else if (!this.isValidAmount) {
      this.validateAmount();
    } else {
      this.validatePIN();
    }
  }

  resetInput() {
    this.inputString = '';
    this.inputValue = '';
  }

  setMessage(message: string) {
    this.message = message;
  }

  inValidCard() {
    this.resetInput();
    this.setMessage('InValid Card Number');
    setTimeout(() => {this.terminateTransaction(); }, 3000);
    throw new Error('InValid Card Number');
  }
}
