import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler} from '@angular/core';
import {CustomErrorHandlerService} from './custom-error-handler.service';
import { AppComponent } from './app.component';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppService } from './Providers/appService/app.service';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService } from './Providers/api/api.service';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      preventDuplicates: true,
      closeButton: true,
      progressBar: true,
      progressAnimation: 'increasing'
    })
  ],
  providers: [
    ApiService,
    AppService,
    HttpClient,
    CustomErrorHandlerService,
    {
      provide: ErrorHandler,
      useClass: CustomErrorHandlerService,
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
