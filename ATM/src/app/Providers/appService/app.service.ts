import { CardDetail } from './../../Models/CardDetails';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError} from 'rxjs';
import { map, catchError, timeout} from 'rxjs/operators';
import { CustomErrorHandlerService } from 'src/app/custom-error-handler.service';

@Injectable()
export class AppService {
  private maxTimeout = 5000;
  apiCallCount = 0;
  constructor(private http: HttpClient, private customErrorHandler: CustomErrorHandlerService) {
  }
  headers = new HttpHeaders();
  putAction(path: string, data: CardDetail, body) {
    this.headers = this.headers.set('ATMBalance', String(body.ATMBalance))
                    .set('cashWithDrawal', String(body.cashWithDrawal))
                    .set('PINStatus', String(body.PINStatus)) ;
    const httpOptions = {
      headers : this.headers
    };
    return this.http.put(path, data, httpOptions)
    .pipe(timeout(this.maxTimeout), map(res => {
        return this.extractData(res);
      }),
      catchError(err => {
        return this.customErrorHandler.handleError(err);
      }));
  }

  getAction(path: string) {
    return this.http.get(path)
    .pipe(timeout(this.maxTimeout), map(res => {
      return this.extractData(res);
      }),
      catchError(err => {
        return this.customErrorHandler.handleError(err);
      }));
  }

  private extractData(res: any) {
    if (res && res.status === 200) {
      return res.json() || {};
    } else {
      return res || {};
    }
  }
}
