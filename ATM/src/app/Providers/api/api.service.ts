import { CardDetail } from './../../Models/CardDetails';
import { Injectable } from '@angular/core';
import { AppService } from '../appService/app.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private appService: AppService) { }

  getCardDetails(cardNumber) {
    return this.appService.getAction('https://localhost:5001/api/CardDetails/' +  cardNumber);
  }

  updateCarddetails(cardDetail: CardDetail, body) {
    return this.appService.putAction('https://localhost:5001/api/CardDetails/' + cardDetail.cardNumber, cardDetail, body);
  }
}
