using System.ComponentModel.DataAnnotations;

namespace ATMWebAPI.Models{
    public class CardDetails {
        [Required]
        [Key]
        public string cardNumber{get; set;}
        [Required]
        public string ownerName{get; set;}
        [Required]
        public float balance{get;set;}
        [Required]
        public string status{get;set;}
        [Required]
        public int PIN{get;set;}
        public int countOfInValidPIN {get; set;}
        
    }
}