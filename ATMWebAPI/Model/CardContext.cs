using Microsoft.EntityFrameworkCore;

namespace ATMWebAPI.Models
{
    public class CardContext: DbContext
    {
        public CardContext(DbContextOptions<CardContext> options)
           : base(options)
        {
        }

        public DbSet<CardDetails> CardDetails { get; set; }
    }
}