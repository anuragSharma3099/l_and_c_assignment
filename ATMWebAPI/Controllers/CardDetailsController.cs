using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ATMWebAPI.Models;
using Microsoft.AspNetCore.Cors;
using ATMWebAPI.ATMException;
using System.Web.Http.Results;
using Microsoft.Extensions.Primitives;
namespace ATMWebAPI.Controllers
{
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class CardDetailsController: ControllerBase
    {
        private readonly ICardDetailRepository _cardRepo;

        public CardDetailsController(ICardDetailRepository cardRepo)
        {
            _cardRepo = cardRepo;
        }

        [HttpGet("{id}")]
        public IActionResult GetCardDetails(string id)
        {
            try{
            var cardDetails = _cardRepo.GetCardDetail(id);
            return Ok(cardDetails);
            }
            catch(InvalidCardNumberException ex){
                return NotFound(ex.Message);
            }
            catch(CardBlockedException ex){
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        public  IActionResult UpdateCardDetails(string id, CardDetails cardDetail)
        {
            float ATMBalance = float.Parse(Request.Headers["ATMBalance"]);
            float cashWithDrawal = float.Parse(Request.Headers["cashWithDrawal"]);
            bool PINStatus = bool.Parse(Request.Headers["PINStatus"]);
            if(ATMBalance < 0) {
                try {
                    _cardRepo.ValidateAmount(cardDetail,cashWithDrawal);
                    return Ok();
                }
                catch(InvalidAmountException ex){
                    return BadRequest(ex.Message);
                }
            }
            else{
                try {
                    _cardRepo.UpdateCardDetail(cardDetail, ATMBalance, cashWithDrawal, PINStatus);
                    return Ok();
                }
                catch(InvalidPinException ex){
                    return BadRequest(ex.Message);
                }
                catch(ToManyInvalidPinException ex) {
                    return BadRequest(ex.Message);
                }
                catch(InsufficientBalanceException ex) {
                    return BadRequest(ex.Message);
                }
                catch(InsufficientBalanceInATMException ex) {
                    return BadRequest(ex.Message);
                }
            }
        }
    }
}
