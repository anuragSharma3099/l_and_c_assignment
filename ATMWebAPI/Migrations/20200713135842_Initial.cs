﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ATMWebAPI.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CardDetails",
                columns: table => new
                {
                    cardNumber = table.Column<string>(nullable: false),
                    ownerName = table.Column<string>(nullable: false),
                    balance = table.Column<float>(nullable: false),
                    status = table.Column<string>(nullable: false),
                    PIN = table.Column<int>(nullable: false),
                    countOfInValidPIN = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardDetails", x => x.cardNumber);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardDetails");
        }
    }
}
