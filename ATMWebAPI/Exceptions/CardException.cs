using System;

namespace ATMWebAPI.ATMException {
    public class CardException : Exception {
        public CardException(string errorMessage) : base(errorMessage) { }
    }

    public class InvalidCardNumberException : CardException {
        public InvalidCardNumberException(string errorMessage) : base(errorMessage) { }
    }

    public class CardBlockedException : CardException {
        public CardBlockedException(string errorMessage) : base(errorMessage) { }
    }

    public class InvalidPinException : CardException {
        public InvalidPinException(string errorMessage) : base(errorMessage) { }
    }

    public class InsufficientBalanceException : CardException {
        public InsufficientBalanceException(string errorMessage) : base(errorMessage) { }
    }

    public class ToManyInvalidPinException : CardException {
        public ToManyInvalidPinException(string errorMessage) : base(errorMessage) { }
    }
}