using System;

namespace ATMWebAPI.ATMException {
    public class ATMException : Exception {
        public ATMException(string errorMessage) : base(errorMessage) { }
    }

    public class InsufficientBalanceInATMException : ATMException {
        public InsufficientBalanceInATMException(string errorMessage) : base(errorMessage) { }
    }

    public class InvalidAmountException : ATMException {
        public InvalidAmountException(string errorMessage): base(errorMessage) { }
    }
}