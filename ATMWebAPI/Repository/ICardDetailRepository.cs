using System.Collections.Generic;
using System.Linq;

namespace ATMWebAPI.Models
{
    public interface ICardDetailRepository
    {
        CardDetails GetCardDetail(string cardNumber);
        CardDetails UpdateCardDetail(CardDetails cardDetail, float ATMBalance, float amount, bool PINStatus);
        CardDetails ValidateAmount(CardDetails cardDetails, float amount);
    }
}