using System.Transactions;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ATMWebAPI.ATMException;

namespace ATMWebAPI.Models 
{
    public class CardDetailRepository: ICardDetailRepository
    {
        private readonly CardContext dbContext;
        public CardDetailRepository(CardContext dbContext){
            this.dbContext = dbContext;
        }

        public CardDetails GetCardDetail(string cardNumber) {
            CardDetails cardDetail = dbContext.CardDetails.Find(cardNumber);
            if(cardDetail == null) {
                throw new InvalidCardNumberException("Invalid Card Number");
            }
            if(cardDetail.status == "blocked") {
                throw new CardBlockedException("Card Is blocked");
            }
            return cardDetail;
        }

        public CardDetails UpdateCardDetail(CardDetails cardDetail, float ATMBalance, float cashWithdrawal, bool PINStatus) {
            if(!PINStatus) {
                this.InvalidPIN(cardDetail);
            }
            CheckForSufficientBalance(cardDetail.balance, ATMBalance, cashWithdrawal);
            cardDetail.balance -= cashWithdrawal;
            cardDetail.countOfInValidPIN = 0;
            var card= dbContext.CardDetails.Update(cardDetail);
            card.State= Microsoft.EntityFrameworkCore.EntityState.Modified;
            dbContext.SaveChanges();
            return cardDetail;
        }
        public void CheckForInvalidAttempts(CardDetails cardDetail) {
            if(cardDetail.countOfInValidPIN == 3) {
                cardDetail.status = "blocked";
                cardDetail.countOfInValidPIN = 0;
                var card= dbContext.CardDetails.Update(cardDetail);
                card.State= Microsoft.EntityFrameworkCore.EntityState.Modified;
                dbContext.SaveChanges();
                throw new ToManyInvalidPinException("To many Invalid PIN. Card Blocked");
            }
        }

        public void InvalidPIN(CardDetails cardDetail) {
            cardDetail.countOfInValidPIN += 1;
            this.CheckForInvalidAttempts(cardDetail);
            var card= dbContext.CardDetails.Update(cardDetail);
            card.State= Microsoft.EntityFrameworkCore.EntityState.Modified;
            dbContext.SaveChanges();
            throw new InvalidPinException("InValid PIN");
        }

        public void CheckForSufficientBalance(float balance, float ATMBalance, float cashWithdrawal) {
            if(ATMBalance < cashWithdrawal){
                throw new InsufficientBalanceInATMException("Insufficient cash in ATM");
            }
            if(balance < cashWithdrawal){
                throw new InsufficientBalanceException("Insufficient balance in your Account");
            }
        }

        public CardDetails ValidateAmount(CardDetails cardDetails, float amount) {
            if(amount % 100 != 0) {
                throw new InvalidAmountException("Invalid Amount");
            }
            return cardDetails;
        }
    }
}