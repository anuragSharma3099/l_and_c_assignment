public class Wallet { 
    private float totalMoney;
    public Wallet(){
        this.totalMoney = 20.00f;
    } 
    public float getTotalMoney() { 
        return this.totalMoney; } 
    public void setTotalMoney(float newtotalMoney) { 
        this.totalMoney = newtotalMoney; } 
    public void addMoney(float deposit) { 
        this.totalMoney += deposit; } 
    public void subtractMoney(float debit) { 
        this.totalMoney -= debit; } 
}