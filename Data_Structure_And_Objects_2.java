// public class Customer { 
//     private String firstName; 
//     private String lastName; 
//     private Wallet myWallet; 
//     public String getFirstName(){ 
//         return firstName; 
//     } 
//     public String getLastName(){ 
//         return lastName; 
//     } 
//     public Wallet getWallet(){ 
//         return myWallet; 
//     } 
// } 
// public class Wallet { 
//     private float value; 
//     public float getTotalMoney() { 
//         return value; } 
//     public void setTotalMoney(float newValue) { 
//         value = newValue; } 
//     public void addMoney(float deposit) { 
//         value += deposit; } 
//     public void subtractMoney(float debit) { 
//         value -= debit; } 
// } 

// Client code…. assuming some delivery boy wants to get his payment 
// code from some method inside the delivery boy class... payment = 2.00; 
// “I want my two dollars!” 
// Wallet theWallet = myCustomer.getWallet(); 
// if (theWallet.getTotalMoney() > payment) { 
//     theWallet.subtractMoney(payment); } 
//     else { // come back later and get my money }


// This approach is violating the Law of demeter.
// The Law of Demeter says that a method f of a class C should only call the methods of these:
// • C 
// • An object created by f
// • An object passed as an argument to f 
// • An object held in an instance variable of C
// The method should not invoke methods on objects that are returned by any of the allowed functions
// In this, approach we are calling methods of Wallet class by the object returned by getWallet() method
// of Customer class, which is violating the law of demeter.
// The problem in this approach is that I am exposing my whole wallet to the delivery boy. I am exposing my
// total amount that I had in my wallet.
// This problem can be solved by making a method which will check that I have amount equall or greater than payment in my wallet.
// If i have that amount then I will pay it else i will say come back later and get your money.
// Instead of exposing the whole wallet class to delivery boy I can expose that method only.
class InSufficientBalanceException extends Exception{
    public InSufficientBalanceException(String exceptionMessage){
        super(exceptionMessage);
    }
}

public class Customer { 
    private String firstName; 
    private String lastName; 
    private Wallet myWallet;
    public Customer(){
        this.myWallet = new Wallet();
    }
    public String getFirstName(){ 
        return firstName; 
    } 
    public String getLastName(){ 
        return lastName; 
    } 
    public void getPayment(float payment){
        String comeBackLaterMessage = "come back later and get your money"
        try{
            if(myWallet.getTotalMoney() >= payment){
                myWallet.subtractMoney(payment);
                System.out.println("Payment done successfully");
            }
            else{
            throw new InSufficientBalanceException(comeBackLaterMessage);
            }
        }
        catch(InSufficientBalanceException exception){
            System.out.println(comeBackLaterMessage);
        }
    } 
}

public class Wallet { 
    private float totalMoney; 
    public float getTotalMoney() { 
        return totalMoney; } 
    public void setTotalMoney(float newtotalMoney) { 
        totalMoney = newtotalMoney; } 
    public void addMoney(float deposit) { 
        totalMoney += deposit; } 
    public void subtractMoney(float debit) { 
        totalMoney -= debit; } 
}

public class DeliveryBoy{
    public static void main() {
        myCustomer = new Customer();
        float payment = 2.00;
        float amountPaid = myCustomer.getAmountPaid(payment);
        if(amountPaid == payment){
            //Payment done successfully
        }
        else{
            // come back later and get my money
        }
    }
}