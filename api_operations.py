#importing requests for api call
import json
import requests

def get_response(blog_name, api_parameter):
    url = 'https://' + blog_name + '.tumblr.com/api/read/json'
    response = requests.get(url, api_parameter)
    return response

def convert_to_valid_json(response):
    response = response.text
    #getting start and end index of substring which is valid json
    start_index = response.find('{')
    end_index = response.rindex('}')
    #removing invalid substring from start and end
    response = response[start_index:end_index + 1]
    return json.loads(response)
    
def generate_api_parameter(min_range, max_range):
    return {
        'type':'photo',
        'num':max_range - min_range + 1,
        'start':min_range - 1
    }
