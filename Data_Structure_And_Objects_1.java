//Q. 1) 

Class Employee { 
    String name; 
    int age; 
    float salary; 
    public: 
        String getName(); 
        void setName(string name); 
        int getAge(); 
        void setAge(int age); 
        float getSalary(); 
        void setSalary(float salary); 
};
Employee employee;

// employee is a Data Structure(Data Transfer Object) as it is exposing only data (name, salary and age) of an Employee 
// not any significant behaviour of Employee. If it expose any behaviour of an Employee like calculatetax()
// method which will return the amount of tax to be paid by an Employee and don't expose any data by getters
// and setters then it will be an object.


