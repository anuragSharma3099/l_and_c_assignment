#importing required functions from apiOperations
from api_operations import get_response, convert_to_valid_json, generate_api_parameter

def blog_name_input():
    blog_name_input_message = "enter the Tumblr blog name: "
    blog_name = input(blog_name_input_message)
    return blog_name

def range_input():
    range_input_message = "enter the range: "
    range_points = input(range_input_message)
    range_endpoints = range_points.split(' ')
    return int(range_endpoints[0]), int(range_endpoints[2])

def is_valid_range(json_response, min_range, max_range):
    if min_range < 1 or max_range > json_response['posts-total']:
        print("Enter Valid Range")
        return False
    return True

def is_blog_available(json_response):
    if json_response['posts-total']:
        return True
    print('There is no such blog')
    return False

def is_valid(response, min_range, max_range):
    if is_blog_available(response) and is_valid_range(response, min_range, max_range):
        return True
    return False

def show_basic_information(json_response):
    print('title: ', json_response['tumblelog']['title'])
    print('name: ', json_response['tumblelog']['name'])
    print('description: ', json_response['tumblelog']['description'])
    print('no of post: ', json_response['posts-total'])

def show_image_urls(min_range, max_range, response, blog_name):
    highest_image_quality = 'photo-url-1280'
    post_number = min_range
    for post in response['posts']:
        #printing image url which has image quality of 1280
        print(post_number, ': ', post[highest_image_quality])
        for photo in post['photos']:
            #checking the duplication of image url
            if photo[highest_image_quality] != post[highest_image_quality]:
                print('     ', photo[highest_image_quality])
        post_number += 1
    #fetching the data after 50 posts
    if post_number <= max_range:
        #generating api_parameter for next set of posts
        api_parameter = generate_api_parameter(post_number - 1, max_range)
        json_response = valid_response(blog_name, api_parameter)
        show_image_urls(post_number-1, max_range, json_response, blog_name)

def valid_response(blog_name, api_parameter):
    response = get_response(blog_name, api_parameter)
    json_response = convert_to_valid_json(response)
    return json_response

def show_details(json_response, min_range, max_range, blog_name):
    if is_valid(json_response, min_range, max_range):
        show_basic_information(json_response)
        show_image_urls(min_range, max_range, json_response, blog_name)

def main():
    blog_name = blog_name_input()
    min_range, max_range = range_input()
    api_parameter = generate_api_parameter(min_range, max_range)
    json_response = valid_response(blog_name, api_parameter)
    show_details(json_response, min_range, max_range, blog_name)

main()
